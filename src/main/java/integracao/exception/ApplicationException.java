package integracao.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ApplicationException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6763638857845136324L;
	private String origem;
	private Integer codigo;
	private String mensagem;
	private String codSituacao;
	private String codBarramento;
	private String sqlCodeJava;

	public ApplicationException(String origem, Integer codigo, String messagem, String msg) {
		super(msg);
		this.origem = origem;
		this.codigo = codigo;
		this.mensagem = messagem;
	}

	public ApplicationException(String codSituacao, String mensagem, String codBarramento, String sqlCodeJava, String msg) {
		super(msg);
		this.codSituacao = codSituacao;
		this.mensagem = mensagem;
		this.codBarramento = codBarramento;
		this.sqlCodeJava = sqlCodeJava;
	}

	public ApplicationException(Throwable cause) {
		super(cause);
	}

	public ApplicationException(String mensagem, String cause) {
		super(cause);
		this.mensagem = mensagem;
	}

	public ApplicationException(String mensagem) {
		super(mensagem);
	}

}
