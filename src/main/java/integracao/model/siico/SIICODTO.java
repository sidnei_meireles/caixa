package integracao.model.siico;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SIICODTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private BigInteger codigo;
	private String nome_produto;
	private String descricao_comercial;
	private String ultima_situacao;
	private BigInteger codigo_unidade_gestora;
	private BigInteger numero_natural_gestora;
	private String sigla_sistema;
	private List<PublicoAlvo>publico_alvo;

}
