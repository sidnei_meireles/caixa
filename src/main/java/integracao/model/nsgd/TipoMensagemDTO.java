package integracao.model.nsgd;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class TipoMensagemDTO {
	private String descricao;
	private MensagemDTO mensagem;
}
