package integracao.model.nsgd;

import lombok.Getter;
import lombok.Setter;

/***
 * Nome: DebitoAdicionalDTO
 *
 * Descrição: Essa classe DTO pertence ao grupo dos campos de requisicao para
 * efetuar um debito no nsgd.
 * 
 * Debitos adicionais possiveis para o debito solicitado.
 * 
 * Métodos: Getter e Setter via Lombok
 *
 * Alterações: Nao houve alteracao
 *
 * @author F609742 - Mike Lucas Souza Paiva
 */
@Getter
@Setter
public class DebitoAdicionalDTO {
	private CifDTO cif;
	private Double valor_minimo;
	private Double valor;
}