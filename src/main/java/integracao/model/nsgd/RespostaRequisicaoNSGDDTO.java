package integracao.model.nsgd;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespostaRequisicaoNSGDDTO {
	private Integer nsu;
	private Integer agencia_vigente;
	private String nsu_barramentoNsu;
	private Double valor_efetivado;
	private Double valor_tarifa;
	private String msg;

	@Override
	public String toString() {
		return "RespostaRequisicao [nsu=" + nsu + ", agencia_vigente=" + agencia_vigente + ", nsu_barramentoNsu="
				+ nsu_barramentoNsu + ", valor_efetivado=" + valor_efetivado + ", valor_tarifa=" + valor_tarifa + "]";
	}

}
