package integracao.model.nsgd;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MensagemDTO {

	private String origem;
	private Integer codigo;
	private String mensagem;
}
