package integracao.model.nsgd;

import lombok.Getter;
import lombok.Setter;

/***
 * Nome: CifDTO
 *
 * Descri��o: Essa classe DTO pertence ao grupo dos campos de requisicao para
 * efetuar um debito no nsgd.
 * 
 * CIF - Identificador interno utilizado pela Caixa NSGD para orquestracao de
 * seus processos, ele define o tipo de lancamento, a funcionalidade e o canal
 *
 * M�todos: Getter e Setter via Lombok
 *
 * Altera��es: Nao houve alteracao
 *
 * @author F609742 - Mike Lucas Souza Paiva
 */
@Getter
@Setter
public class CifDTO {
	private Integer acao;
	private Integer modo;
	private Integer grupo;
	private Integer subgrupo;
	private Integer tipo;
	private Integer subtipo;
}
