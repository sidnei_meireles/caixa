package integracao.model.nsgd;

import lombok.Getter;
import lombok.Setter;

/***
 * Nome: OrigemDTO
 *
 * Descrição: Essa classe DTO pertence ao grupo dos campos de requisicao para
 * efetuar um debito no nsgd.
 * 
 * Dados do canal origem que esta transacionando com NSGD.
 *
 * Métodos: Getter e Setter via Lombok
 *
 * Alterações: Nao houve alteracao
 *
 * @author F609742 - Mike Lucas Souza Paiva
 */
@Getter
@Setter
public class OrigemDTO {
	private Integer agencia;
	private Integer segmento;
	private String nsu;
	private Integer canal;
	private String transacao;
}
