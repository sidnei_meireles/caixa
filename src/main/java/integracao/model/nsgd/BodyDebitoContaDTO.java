package integracao.model.nsgd;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/***
 * Nome: BodyDebitoContaDTO
 *
 * Descrição: Essa classe DTO representa o grupo dos campos de requisicao para
 * efetuar um debito no nsgd.
 *
 * Métodos: Getter e Setter via Lombok
 *
 * Alterações: Nao houve alteracao
 *
 * @author F609742 - Mike Lucas Souza Paiva
 */
@Getter
@Setter
public class BodyDebitoContaDTO {
	private CifDTO cif;
	private String data_movimento;
	private String data_referencia;
	private Double valor;
	private Double valor_minimo;
	private Integer numero_documento;
	private Integer numero_descricao_lancamento;
	private String nome_estabelecimento;
	private Boolean flag_incondicional;
	private List<DebitoAdicionalDTO> debitos_adicionais;
	private OrigemDTO origem;
}
