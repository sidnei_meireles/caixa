package integracao.model.cicli;

import lombok.Getter;
import lombok.Setter;

/***
 * Nome: ClienteDTO
 *
 * Descrição: Essa classe DTO representa atributos de comunicação com CICLI
 *
 * Métodos: Getter e Setter via Lombok
 *
 * Alterações: Nao houve alteracao
 *
 * @author F567286 - Milton França
 */
@Getter
@Setter
public class ClienteDTO {
	private String tipoPessoa;
	private String cpfCnpj;
	private String nomeRazaoSocial;
	private String nomeSocialFantasia;
	private String perfilInvestidor;  //API - PERFIL --> pesquisar no portal API
	private String dataPerfil;
	private String segmento;
	private String carteira;
	private String cnae;
	private String descricaoCnae;
}
