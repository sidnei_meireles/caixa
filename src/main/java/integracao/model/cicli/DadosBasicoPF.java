package integracao.model.cicli;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DadosBasicoPF extends DadosBasicos {
	String marcaDOI;
	String nome;
	String dtNascimento;
	String tpPessoa;
	String nomeMae;
	String nuPaisOrigem;
	String nomeSocial;
	String cocli;
	String nomePai;
	String localidade;
	String sexo;
	String nomeReduzido;
	String estadoCivil;
	String naturalidade;
	String escolaridade;
	String cpfConjuge;
	String nomeConjuge;
	String nomeMaeConjuge;
	String dtNascConjuge;
}
