package integracao.model.cicli.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TipoPessoa {

	FISICA("PF", "Pessoa F�sica"), 
	JURIDICA("PJ", "Pessoa Jur�dica");

	String codigo;
	String tpPessoaFisica;

	public static TipoPessoa obterPorCodigo(Integer codigo) {
		TipoPessoa tipo = null;
		for (TipoPessoa tipoSelecionado : TipoPessoa.values()) {
			if (tipoSelecionado.codigo.equals(codigo)) {
				tipo = tipoSelecionado;
			}
		}
		return tipo;
	}

}
