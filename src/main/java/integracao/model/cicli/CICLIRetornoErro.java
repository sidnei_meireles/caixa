package integracao.model.cicli;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CICLIRetornoErro {
private Integer codigo;
private String descricao;
private String orientacao;
}
