package integracao.model.cicli;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RetornoCICLIDTO {
	String codSituacao;
	String mensagem;
	String codBarramento;
	String sqlCodeJava;
}
