package integracao.model.cicli;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DadosBasicosPJ extends DadosBasicos {
	private String marcaDOI;
	private String razaoSocial;
	private String nomeFantasia;
	private String dtConstituicao;
	private Double capitalSocial;
	private String concla;
	private String naturezaJuridica;
	private String cnaev1;
	private Integer cnaev2;
	private Integer cnaev3;
	private Integer cnaev4;
	private Integer cnaev5;
	private String cocli;
	private String noReduzido;
	private String matrizFilial;
	private String tpFimLucrativo;
	private String tpFranquia;
	private Integer nuRegimeTributario;
	private String deRegimeTributario;
}
