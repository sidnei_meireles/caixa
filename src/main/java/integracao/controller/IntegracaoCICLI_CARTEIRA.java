package integracao.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;

import integracao.exception.ApplicationException;
import integracao.model.cicli.CICLIRetornoErro;
import integracao.model.cicli.ClienteDTO;
import integracao.model.cicli.RetornoCICLIDTO;
import lombok.extern.java.Log;
@Log
public class IntegracaoCICLI_CARTEIRA {

	// private static String URL = "https://api.des.caixa/cadastro/v2";
	//campos=dadosbasicos&cpfcnpj=
	private static String APIKEY = "l7c5fc4112735c4687b62c6e8deeab6ecf";

	public static ClienteDTO efetuarRequisicaoCICLI(String token, String cnpjCpf, String tipoPessoa)
			throws ApplicationException, ParseException, IOException, JSONException, URISyntaxException {
		
		String pessoa = tipoPessoa.equals("1") ? "tipoPessoa": "pessoas-juridicas";
		
		String URL = "https://api.des.caixa:8443/investimentos/perfil-investidor/v3/investidores/"+pessoa+"/"+cnpjCpf+"/perfil";
		ClienteDTO respostaRequisicao = null;
		String result = null;
		HttpEntity entity = null;
		Gson gson = new Gson();
		CloseableHttpResponse response = null;

		URIBuilder builder = new URIBuilder(URL);
		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet getRequest = new HttpGet(builder.build());
		
		getRequest.setHeader("Authorization", "Bearer " + token);
		getRequest.setHeader("Accept", "application/json");
		getRequest.setHeader("apikey", APIKEY);
		response = client.execute(getRequest);
		entity = response.getEntity();
		result = EntityUtils.toString(entity);
		JSONObject jb = new JSONObject(result);
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
			CICLIRetornoErro erro =  gson.fromJson(jb.toString(), CICLIRetornoErro.class);
			throw new ApplicationException(erro.getDescricao());
		}
		if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			RetornoCICLIDTO dto = new RetornoCICLIDTO();
			JSONObject jbo = (JSONObject) jb.get("retorno");
			dto = gson.fromJson(jbo.toString(), RetornoCICLIDTO.class);
			throw new ApplicationException(dto.getCodSituacao(), dto.getMensagem(), dto.getCodBarramento(),
					dto.getSqlCodeJava(), dto.getMensagem());
		}

		JSONArray jbo = (JSONArray) jb.get("datavalidade");
		
		respostaRequisicao = gson.fromJson(jbo.toString(), ClienteDTO.class);
		return respostaRequisicao;
	}

	public static void main(String[] args) {
		IntegracaoUtil util = new IntegracaoUtil();
		try {
		String token = util.getToken();
		String cnpjCpf = "00103867503";
			efetuarRequisicaoCICLI(token, "1" ,cnpjCpf);
		} catch (ApplicationException | ParseException | IOException | JSONException | URISyntaxException e) {
			log.warning("Erro ao obter a transa��o "+ e.getMessage());
		}
	}
}
