package integracao.controller;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import integracao.exception.ApplicationException;
import integracao.model.siico.SIICODTO;
import lombok.extern.java.Log;

@Log
public class IntegracaoSIICO {

	// private static String URL = "https://api.des.caixa/cadastro/v2";
	private static String URL = "https://api.des.caixa:8443/informacoes-corporativas-privadas/v1/produtos";
	private static String APIKEY = "l7c5fc4112735c4687b62c6e8deeab6ecf";

	public static List<SIICODTO> efetuarRequisicaoCICLI(String token) throws ApplicationException, ParseException, IOException, JSONException {
		List<SIICODTO> respostaRequisicao = null;
		String result = null;
		HttpEntity entity = null;
		Gson gson = new Gson();
		CloseableHttpResponse response = null;
		String path = URL;

		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet getRequest = new HttpGet(path);

		getRequest.setHeader("Authorization", "bearer " + token);
		getRequest.setHeader("apikey", APIKEY);
		response = client.execute(getRequest);
		entity = response.getEntity();
		result = EntityUtils.toString(entity).replaceAll("-","_").trim().replaceAll("\\s+", " ");
		JSONArray jb = new JSONArray(result);
		TypeToken tt = new TypeToken<List<SIICODTO>>() {};
		respostaRequisicao =  gson.fromJson(jb.toString(), tt.getType());
	if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			throw new ApplicationException("Erro ao efetuar a consulta no sistema SIICO");
		}
		return respostaRequisicao;
	}

	public static void main(String[] args) {
		IntegracaoUtil util = new IntegracaoUtil();
		try {
			String token = util.getToken();
			// String cnpjCpf = "37460960578";
			String cnpjCpf = "02046600000116";
			efetuarRequisicaoCICLI(token);
		} catch (ApplicationException | ParseException | IOException | JSONException e) {
			log.warning("Erro ao obter a transa��o " + e.getMessage());
		}
	}
}
