package integracao.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;

import integracao.exception.ApplicationException;
import integracao.model.cicli.DadosBasicoPF;
import integracao.model.cicli.DadosBasicos;
import integracao.model.cicli.DadosBasicosPJ;
import integracao.model.cicli.RetornoCICLIDTO;
import integracao.model.cicli.enums.TipoPessoa;
import lombok.extern.java.Log;

@Log
public class IntegracaoCICLI_NATIVO {

	// private static String URL = "https://api.des.caixa/cadastro/v2";
	// campos=dadosbasicos&cpfcnpj=
	private static String URL = "http://api.des.caixa:8080";
	private static String APIKEY = "l7c5fc4112735c4687b62c6e8deeab6ecf";
	private final static String UTF8 = "UTF-8";

	public static DadosBasicos efetuarRequisicaoCICLI(String token, String cnpjCpf)
			throws ApplicationException, ParseException, IOException, JSONException, URISyntaxException {
		URL url = new URL(URL + "/cadastro/v2/clientes?" + "campos=dadosbasicos&cpfcnpj=" + cnpjCpf);

		DadosBasicos respostaRequisicao = null;
		Gson gson = new Gson();
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Authorization", "bearer " + token);
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestProperty("apiKey", "l7c5fc4112735c4687b62c6e8deeab6ecf");

		if (conn.getResponseCode() == HttpStatus.SC_UNAUTHORIZED || conn.getResponseCode() != HttpStatus.SC_OK) {
			getError(gson, conn);
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), UTF8));
		String response = br.readLine();
		JSONObject jb = new JSONObject(response);

		JSONObject jbo = (JSONObject) jb.get("dadosBasicos");
		String tpPessoa = jbo.getString("tipoPessoa");
		if (tpPessoa.equals(TipoPessoa.FISICA.getCodigo())) {
			respostaRequisicao = gson.fromJson(jbo.toString(), DadosBasicoPF.class);
		} else {
			respostaRequisicao = gson.fromJson(jbo.toString(), DadosBasicosPJ.class);
		}
		conn.disconnect();
		return respostaRequisicao;
	}

	private static void getError(Gson gson, HttpURLConnection conn)
			throws UnsupportedEncodingException, IOException, JSONException, ApplicationException {
		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getErrorStream(), UTF8));
		String response = br.readLine();
		RetornoCICLIDTO dto = new RetornoCICLIDTO();
		JSONObject jbr = null;
		String mensagem = null;
		String erro = null;
		JSONObject jb = new JSONObject(response);
		if (!jb.isNull("retorno")) {
			jbr = (JSONObject) jb.get("retorno");
		}
		if (!jb.isNull("mensagem")) {
			mensagem = (String) jb.get("mensagem");
		}
		if (!jb.isNull("erro")) {
			erro = (String) jb.get("erro");
		}
		dto = jbr == null ? getRetornoCicliDTO(mensagem, erro): gson.fromJson(jbr.toString(), RetornoCICLIDTO.class);
		throw new ApplicationException(dto.getCodSituacao(), dto.getMensagem(), dto.getCodBarramento(),
				dto.getSqlCodeJava(), dto.getMensagem());
	}

	private static RetornoCICLIDTO getRetornoCicliDTO(String mensagem, String erro) {
		RetornoCICLIDTO ret = new RetornoCICLIDTO();
		if(mensagem!=null) {
			ret.setMensagem(mensagem);
		}
		if(erro!=null) {
			ret.setCodSituacao(erro); 
		}
		return ret;
	}

	public static void main(String[] args) {
		IntegracaoUtil util = new IntegracaoUtil();
		try {
			String token = util.getToken();
			// String cnpjCpf = "37460960578";
			String cnpjCpf = "02046600000116";
			efetuarRequisicaoCICLI(token, cnpjCpf);
		} catch (ApplicationException | ParseException | IOException | JSONException | URISyntaxException e) {
			log.warning("Erro ao obter a transa��o " + e.getMessage());
		}
	}
}
