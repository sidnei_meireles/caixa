package integracao.controller;

import java.io.IOException;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;

import integracao.exception.ApplicationException;
import integracao.model.nsgd.BodyDebitoContaDTO;
import integracao.model.nsgd.CifDTO;
import integracao.model.nsgd.MensagemDTO;
import integracao.model.nsgd.OrigemDTO;
import integracao.model.nsgd.RespostaRequisicaoNSGDDTO;
import integracao.model.nsgd.TipoMensagemDTO;
import lombok.extern.java.Log;

@Log
public class IntegracaoNSGD {

	/**
	 * Servi�o de Lancamento de debito em uma conta Caixa - NSGD
	 * 
	 * @throws ApplicationException
	 * @throws IOException
	 * @throws ParseException
	 * @throws JSONException
	 */
	public static RespostaRequisicaoNSGDDTO debitoNSGD(String token, String id_conta, BodyDebitoContaDTO bodyDebito)
			throws ApplicationException, IOException, JSONException {
		MensagemDTO msg = null;
		String result = null;
		HttpEntity entity = null;
		Gson gson = new Gson();
		CloseableHttpResponse response = null;
		String hostBasePath = "https://des.barramento.caixa" + "/sibar/conta-lancamentos";
		String path = "/v1/contas/" + id_conta + "/debitar";

		String body = gson.toJson(bodyDebito);
		CloseableHttpClient client = HttpClients.createDefault();
		HttpPost postRequest = new HttpPost(hostBasePath + path);

		StringEntity request = new StringEntity(body);
		postRequest.setHeader("Content-type", "application/json");
		postRequest.setHeader("Content-type", "text/plain");
		postRequest.setHeader("Authorization", "bearer " + token);
		postRequest.setEntity(request);
		response = client.execute(postRequest);
		//response.setStatusCode(HttpStatus.SC_SERVICE_UNAVAILABLE); 
		if (response.getStatusLine().getStatusCode()== HttpStatus.SC_SERVICE_UNAVAILABLE) {
		throw new ApplicationException("Erro de comunica��o", "O Servi�o de coumunica��o ao NSGD est� indispon�vel");
	}
		entity = response.getEntity();
		result = EntityUtils.toString(entity);
		RespostaRequisicaoNSGDDTO resp = gson.fromJson(result, RespostaRequisicaoNSGDDTO.class);
		TipoMensagemDTO tpmsg = new TipoMensagemDTO();

		if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			JSONObject retorno = new JSONObject(result);
			Map<Object, Object> mp = retorno.toMap();

			for (Map.Entry<Object, Object> o : mp.entrySet()) {
				String k = (String) o.getKey();
				
				if (o.getValue() instanceof JSONObject) {
					JSONObject j = (JSONObject) o.getValue();
					String mesg = j.getString("mensagem");
					String excecao = j.getString("excecao");
					msg = new MensagemDTO();
					msg.setMensagem(mesg);
					tpmsg.setMensagem(msg);
					
					throw new ApplicationException(msg.getMensagem(), excecao);
				} else {
					JSONArray v = (JSONArray) o.getValue();
					tpmsg.setDescricao(k);
					tpmsg.setMensagem(new MensagemDTO());
					for (int i = 0; i < v.length(); i++) {
						JSONObject obj = (JSONObject) v.get(i);
						msg = gson.fromJson(obj.toString(), MensagemDTO.class);
						tpmsg.setMensagem(msg);
					}
					
					throw new ApplicationException(msg.getOrigem(), msg.getCodigo(), msg.getMensagem(),
							msg.getMensagem());
				}
			}

		}
		return resp;
	}


	public static void main(String[] args) {
		try {
		BodyDebitoContaDTO bodyDebito = getBody();
		IntegracaoUtil util = new IntegracaoUtil();
		String token = util.getToken();
		 String idConta = "1900-3701-400010578-6";
		// sem saldo
		//String idConta = "1900-3701-400010595-6";
			RespostaRequisicaoNSGDDTO resp = debitoNSGD(token, idConta, bodyDebito);
		} catch (ApplicationException | ParseException | IOException | JSONException e) {
			log.warning("Erro ao obter a transa��o " + e.getMessage());
		}
	}

	private static BodyDebitoContaDTO getBody() {
		BodyDebitoContaDTO body = new BodyDebitoContaDTO();
		CifDTO cif = new CifDTO();
		cif.setAcao(1);
		cif.setModo(1);
		cif.setGrupo(2);
		cif.setSubgrupo(48);
		cif.setTipo(282);
		cif.setSubtipo(397);
		body.setCif(cif);
		body.setData_movimento("2020-12-18");
		body.setData_referencia("2020-12-18");
		body.setValor(new Double(10.0));
		body.setValor_minimo(new Double(10.0));
		body.setNumero_documento(123456);
		body.setNome_estabelecimento("Teste Debito");
		body.setNumero_descricao_lancamento(123456);
		body.setFlag_incondicional(false);
		OrigemDTO dtoO = new OrigemDTO();
		dtoO.setAgencia(2);
		dtoO.setSegmento(3274);
		dtoO.setCanal(22);
		dtoO.setTransacao("N1DI");
		dtoO.setNsu("1");
		body.setOrigem(dtoO);
		return body;
	}
}
