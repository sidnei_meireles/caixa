package integracao.controller;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import integracao.exception.ApplicationException;
import integracao.model.nsgd.RespostaRequisicaoNSGDDTO;
import integracao.model.siico.SIICODTO;
import lombok.extern.java.Log;

@Log
public class IntegracaoSIICO_PRODUTO {

	private static String URL = "https://api.des.caixa:8443/informacoes-corporativas-privadas/v1/produtos";
	private static String APIKEY = "l7c5fc4112735c4687b62c6e8deeab6ecf";

	@SuppressWarnings("rawtypes")
	public static String efetuarRequisicaoCICLI(String token, String produto) throws ApplicationException, ParseException, IOException, JSONException, URISyntaxException {
		
		List<SIICODTO> respostaRequisicao = null;
		String result = null;
		HttpEntity entity = null;
		Gson gson = new Gson();
		CloseableHttpResponse response = null;

		CloseableHttpClient client = HttpClients.createDefault();

		URIBuilder builder = new URIBuilder(URL);
		builder.setParameter("codigo", produto);
		HttpGet getRequest = new HttpGet(builder.build());

		getRequest.setHeader("Authorization", "bearer " + token);
		getRequest.setHeader("apikey", APIKEY);
		response = client.execute(getRequest);
		entity = response.getEntity();
		result = EntityUtils.toString(entity).replaceAll("-","_").trim().replaceAll("\\s+", " ");
		JSONArray jb = new JSONArray(result);
		TypeToken tt = new TypeToken<List<SIICODTO>>() {};
		respostaRequisicao =  gson.fromJson(jb.toString(), tt.getType());
		if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			throw new ApplicationException("Erro ao efetuar a consulta no sistema SIICO");
		}
		return respostaRequisicao.get(0).getNome_produto();
	}

	public static void main(String[] args) {
		IntegracaoUtil util = new IntegracaoUtil();
		try {
			@SuppressWarnings("static-access")
			String token = util.getToken();
			String produto = "2072";
			String descricao = efetuarRequisicaoCICLI(token, produto);
			System.out.println(descricao);
		} catch (ApplicationException | ParseException | IOException | JSONException | URISyntaxException e) {
			log.warning("Erro ao obter a transa��o " + e.getMessage());
		}
	}
}
