package integracao.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.google.gson.Gson;

import integracao.exception.ApplicationException;
import integracao.model.cicli.DadosBasicoPF;
import integracao.model.cicli.DadosBasicos;
import integracao.model.cicli.DadosBasicosPJ;
import integracao.model.cicli.RetornoCICLIDTO;
import integracao.model.cicli.enums.TipoPessoa;
import lombok.extern.java.Log;
@Log
public class IntegracaoCICLI {

	// private static String URL = "https://api.des.caixa/cadastro/v2";
	//campos=dadosbasicos&cpfcnpj=
	private static String URL = "http://api.des.caixa:8080/cadastro/v1/clientes";
	private static String APIKEY = "l7c5fc4112735c4687b62c6e8deeab6ecf";

	public static DadosBasicos efetuarRequisicaoCICLI(String token, String cnpjCpf)
			throws ApplicationException, ParseException, IOException, JSONException, URISyntaxException {
		String URL = "http://api.des.caixa:8080/cadastro/v2/clientes?";
		DadosBasicos respostaRequisicao = null;
		String result = null;
		HttpEntity entity = null;
		Gson gson = new Gson();
		CloseableHttpResponse response = null;

		URIBuilder builder = new URIBuilder(URL);
		builder
		.setParameter("campos", "dadosbasicos")
		.setParameter("cpfcnpj", cnpjCpf);
		CloseableHttpClient client = HttpClients.createDefault();
		HttpGet getRequest = new HttpGet(builder.build());
		
		getRequest.setHeader("Authorization", "bearer " + token);
		getRequest.setHeader("apikey", APIKEY);
		response = client.execute(getRequest);
		entity = response.getEntity();
		result = EntityUtils.toString(entity);
		JSONObject jb = new JSONObject(result);
		if (response.getStatusLine().getStatusCode() == HttpStatus.SC_UNAUTHORIZED) {
			String mensagem = (String) jb.get("mensagem");
			throw new ApplicationException(mensagem);
		}

		if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			RetornoCICLIDTO dto = new RetornoCICLIDTO();
			JSONObject jbo = (JSONObject) jb.get("retorno");
			dto = gson.fromJson(jbo.toString(), RetornoCICLIDTO.class);
			throw new ApplicationException(dto.getCodSituacao(), dto.getMensagem(), dto.getCodBarramento(),
					dto.getSqlCodeJava(), dto.getMensagem());
		}

		JSONObject jbo = (JSONObject) jb.get("carteiragrc");
		String tpPessoa = jbo.getString("tipoPessoa");
		if (tpPessoa.equals(TipoPessoa.FISICA.getCodigo())) {
			respostaRequisicao = gson.fromJson(jbo.toString(), DadosBasicoPF.class);
		} else {
			respostaRequisicao = gson.fromJson(jbo.toString(), DadosBasicosPJ.class);
		}
		return respostaRequisicao;
	}

	public static void main(String[] args) {
		IntegracaoUtil util = new IntegracaoUtil();
		try {
		String token = util.getToken();
		//String cnpjCpf = "37460960578";
		String cnpjCpf = "02046600000116";
		//String cnpjCpf = "01677051132";
			efetuarRequisicaoCICLI(token, cnpjCpf);
		} catch (ApplicationException | ParseException | IOException | JSONException | URISyntaxException e) {
			log.warning("Erro ao obter a transa��o "+ e.getMessage());
		}
	}
}
